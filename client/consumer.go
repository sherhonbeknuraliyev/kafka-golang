package main

import (
	"fmt"
	"github.com/Shopify/sarama"
)

func main()  {
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	consumer, err := sarama.NewConsumer([]string{"localhost:9092","localhost:9092"}, config)
	if err != nil{
		fmt.Println(err)
		return
	}
	partitions, err := consumer.ConsumePartition("lion",0, sarama.OffsetNewest)
	if err != nil {
		fmt.Println(err)
		return
	}
	res := <-partitions.Messages()

	fmt.Println(string(res.Value))
	topics, err := consumer.Topics()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(topics)

}
