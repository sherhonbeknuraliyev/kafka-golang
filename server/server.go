package main

import (
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"
	"log"
)

func main ()  {
	app := gin.Default()
	app.POST("/", HandleFunc)
	err := app.Run(":9000")
	if err != nil {
		log.Fatalln(err)
	}
}

type Message struct {
	Text string `json:"text"`
}
func HandleFunc(ctx *gin.Context)  {
	var  msg Message
	err := ctx.BindJSON(&msg)
	if err != nil {
		fmt.Println(err)
		ctx.IndentedJSON(400,gin.H{"status":"Bad request"})
		return
	}
	if msg.Text == ""{
		ctx.IndentedJSON(400,gin.H{"status":"Bad request"})
		return
	}
	err = PushCommentToQueue("lion", msg.Text)
	if err != nil {
		fmt.Println(err)
		ctx.IndentedJSON(400,gin.H{"status":"Bad request"})
		return
	}
	
	ctx.IndentedJSON(201, gin.H{"status":"created"})
	return
}
func ConnectToProducer(brokerUrl []string) (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5

	producer, err := sarama.NewSyncProducer([]string{"localhost:9092","localhost:9092"}, config)
	if err != nil{
		return nil, err
	}
	return producer, nil

}
func PushCommentToQueue(topic string, message string) error {
	brokersUrl := []string{"localhost:9092", "localhost:9092"}
	producer, err := ConnectToProducer(brokersUrl)
	if err != nil{
		return err
	}
	msg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(message),
	}
	fmt.Println(message)
	sendMessage, i, err := producer.SendMessage(msg)
	if err != nil {
		return err
	}
	fmt.Println(sendMessage,i)
	return nil
}

func ErrorHandleBadRequest()  {

}